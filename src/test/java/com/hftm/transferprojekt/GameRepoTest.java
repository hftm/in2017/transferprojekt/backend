package com.hftm.transferprojekt;

import com.hftm.transferprojekt.model.Game;
import com.hftm.transferprojekt.model.Level;
import com.hftm.transferprojekt.model.Player;
import com.hftm.transferprojekt.persistence.IGameRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TransferprojektApplication.class)
@EnableJpaRepositories(basePackages={"com.hftm.transferprojekt.persistence"})
@EntityScan(basePackages={"com.hftm.transferprojekt.model"})
@TestPropertySource("classpath:application.properties")
public class GameRepoTest {

    @Qualifier("IGameRepository")
    @Mock
    private static IGameRepository iGameRepository;

    private static Player user = new Player("NelsonTest");

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGameRepo(){
        Date date = new Date(System.currentTimeMillis());
        Game game1 = new Game(5,true,"TestGame", date, Level.EASY, user);
        game1.setGameCode("123");
        iGameRepository.save(game1);
        when(iGameRepository.findGameByGameName("TestGame")).thenReturn(Collections.singletonList(game1));
    }

}
