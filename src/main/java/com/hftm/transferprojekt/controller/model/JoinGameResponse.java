package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.Level;

import java.util.Date;

public class JoinGameResponse {
    private final String gameCode;
    private final String gameName;
    private final Date startDate;
    private final int duration;
    private final Level level;

    public JoinGameResponse(String gameCode, String gameName, Date startDate, int duration, Level level) {
        this.gameCode = gameCode;
        this.gameName = gameName;
        this.startDate = startDate;
        this.duration = duration;
        this.level = level;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getGameName() {
        return gameName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public int getDuration() {
        return duration;
    }

    public Level getLevel() {
        return level;
    }
}
