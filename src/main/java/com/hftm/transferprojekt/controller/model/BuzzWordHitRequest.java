package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.BuzzWord;

public class BuzzWordHitRequest {
    private final String gameCode;
    private final String nickName;
    private final BuzzWord buzzword;

    public BuzzWordHitRequest(String gameCode, String nickName, BuzzWord buzzword){
        this.gameCode = gameCode;
        this.nickName = nickName;
        this.buzzword = buzzword;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }

    public BuzzWord getBuzzword() {
        return buzzword;
    }
}
