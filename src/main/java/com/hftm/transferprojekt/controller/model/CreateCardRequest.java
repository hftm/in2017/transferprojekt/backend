package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.Card;
import com.hftm.transferprojekt.model.Player;

public class CreateCardRequest {
    private final String gameCode;
    private final String nickName;
    private final Card card;

    public CreateCardRequest(String gameCode, String nickName, Card card){
        this.gameCode = gameCode;
        this.nickName = nickName;
        this.card = card;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }

    public Card getCard() {
        return card;
    }
}
