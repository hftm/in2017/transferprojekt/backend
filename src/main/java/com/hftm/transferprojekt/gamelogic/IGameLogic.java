package com.hftm.transferprojekt.gamelogic;

import com.hftm.transferprojekt.exceptions.GameLogicInputException;
import com.hftm.transferprojekt.model.*;
import java.util.Date;

public interface IGameLogic {
    Game newGame(String gameName, boolean needFullCard, int duration, Date startDate, String nickName, Level gameLevel) throws GameLogicInputException;
    Game joinGame(String gameCode, String nickName) throws GameLogicInputException;
    void createCard(String gameCode, String nickName, Card card) throws GameLogicInputException;
    BuzzWord hitBuzzword(String gameCode, String nickName, BuzzWord buzzWord) throws GameLogicInputException;
    Player getPlayer(String gameCode, String nickName);
    boolean playerHasWon(String gameCode, String nickName) throws GameLogicInputException;
    void setWinner(String gameCode, String nickName);
}
