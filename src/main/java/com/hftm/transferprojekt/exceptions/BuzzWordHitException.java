package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class BuzzWordHitException extends RuntimeException {
    public BuzzWordHitException() {
        super();
    }
    public BuzzWordHitException(String message, Throwable cause) {
        super(message, cause);
    }
    public BuzzWordHitException(String message) {
        super(message);
    }
    public BuzzWordHitException(Throwable cause) {
        super(cause);
    }
}
