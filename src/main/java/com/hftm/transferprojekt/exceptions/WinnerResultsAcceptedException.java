package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Error on winnerResultsAccepted")
public class WinnerResultsAcceptedException extends RuntimeException {
    public WinnerResultsAcceptedException() {
        super();
    }
    public WinnerResultsAcceptedException(String message, Throwable cause) {
        super(message, cause);
    }
    public WinnerResultsAcceptedException(String message) {
        super(message);
    }
    public WinnerResultsAcceptedException(Throwable cause) {
        super(cause);
    }
}
