package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Error on starting game")
public class StartGameException extends RuntimeException  {
    public StartGameException() {
        super();
    }
    public StartGameException(String message, Throwable cause) {
        super(message, cause);
    }
    public StartGameException(String message) {
        super(message);
    }
    public StartGameException(Throwable cause) {
        super(cause);
    }
}
