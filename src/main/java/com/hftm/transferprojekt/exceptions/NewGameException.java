package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class NewGameException extends RuntimeException  {
    public NewGameException() {
        super();
    }
    public NewGameException(String message, Throwable cause) {
        super(message, cause);
    }
    public NewGameException(String message) {
        super(message);
    }
    public NewGameException(Throwable cause) {
        super(cause);
    }
}
