package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class JoinGameException extends RuntimeException {
        public JoinGameException() {
            super();
        }
        public JoinGameException(String message, Throwable cause) {
            super(message, cause);
        }
        public JoinGameException(String message) {
            super(message);
        }
        public JoinGameException(Throwable cause) {
            super(cause);
        }
}
