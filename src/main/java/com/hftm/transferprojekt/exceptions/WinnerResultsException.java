package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Error on winnerResults")
public class WinnerResultsException extends RuntimeException {
    public WinnerResultsException() {
        super();
    }
    public WinnerResultsException(String message, Throwable cause) {
        super(message, cause);
    }
    public WinnerResultsException(String message) {
        super(message);
    }
    public WinnerResultsException(Throwable cause) {
        super(cause);
    }
}
