package com.hftm.transferprojekt.persistence;

import com.hftm.transferprojekt.model.Game;
import org.springframework.data.repository.CrudRepository;

public interface IGameRepository extends CrudRepository<Game, String>, IGameRepositoryExtended {
    Game findGameByGameCode(String gameCode);
}
