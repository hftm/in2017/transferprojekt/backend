package com.hftm.transferprojekt.persistence;

import com.hftm.transferprojekt.model.Player;
import org.springframework.data.repository.CrudRepository;

public interface IPlayerRepository extends CrudRepository<Player, String> {
    Player findPlayerByNickName(String nickName);
}
